﻿using UnityEngine;
using System.Collections;

public class Item {

	public string title;
	public int quantity;
	public bool isUnique;
	public int id;

	public Item() {
		this.title = "Collectable_1";
		this.quantity = 1337;
		this.isUnique = false;
		this.id = 2;
	}

	public Item(string title, int number, bool isUnique, int id) {
		this.title = title;
		this.quantity = number;
		this.isUnique = isUnique;
		this.id = id;
	}

	public Item(Item c) {
		this.title = c.title;
		this.quantity = c.quantity;
		this.isUnique = c.isUnique;
		this.id = c.id;
	}

	public bool Equals(Item other) {
		return (this.id == other.id) && !isUnique;
	}
}
