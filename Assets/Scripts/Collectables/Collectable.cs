﻿using UnityEngine;
using System.Collections;


public class Collectable : MonoBehaviour {

	public string title;
	public bool isUnique = false;
	public int quantity;
	public int id;

	public Item item;



	//Constructors
	void Start() {
		item = new Item (title, quantity, isUnique, id);
	}

}
