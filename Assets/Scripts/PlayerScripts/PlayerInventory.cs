﻿using UnityEngine;
using System.Collections;

public class PlayerInventory : MonoBehaviour {

	public Item[] inventory;
	public int[] inventoryQuantity;

	public float offset;

	private int inventorySize;

	private Texture2D inventory_bg_texture;
	private GUIStyle style;
	public float boxSize;


	// Use this for initialization
	void Start () {
		//Load inventory from save-file
		//LoadInventory();
		inventory_bg_texture = new Texture2D(1, 1);
		inventory_bg_texture.SetPixel (0, 0, Color.black);
		inventory_bg_texture.wrapMode = TextureWrapMode.Repeat;
		inventory_bg_texture.Apply ();
		style = new GUIStyle ();
		style.normal.background = inventory_bg_texture;
		offset = 60;
		boxSize = 50;
		initializeInventory ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void LoadInventory() {
		//TODO Load from file;
	}

	public void addToInventory(Item item) {
		for (var i = 0; i < inventorySize; i++) {
			if (inventory [i] == null) {
				inventory [i] = item;
				inventoryQuantity [i] += item.quantity;
				break;
			} else if (inventory [i].Equals (item)) {
				inventoryQuantity [i] += item.quantity;
				break;
			}
		}
	}

	//TODO Replace with LoadInventory
	void initializeInventory() {
		inventorySize = 5;
		inventory = new Item[inventorySize];
		inventoryQuantity = new int[inventorySize];
		for (var i = 0; i < inventorySize; i++) {
			inventory [i] = null;
			inventoryQuantity [i] = 0;

		}
	}

	void OnGUI() {
		for (var i = 0; i < inventorySize; i++) {
			if (inventory [i] != null) {
				Debug.Log (i);
				inventory_bg_texture.SetPixel (0, 0, Color.gray);
				inventory_bg_texture.Apply ();
				GUI.Label(new Rect(10 + (i * offset), 10, 100, 100), inventoryQuantity[i] + "");
			} else {
				inventory_bg_texture.SetPixel (0, 0, Color.black);
				inventory_bg_texture.Apply ();
			}
			//GUI.Box (new Rect (10 + (i * offset), 10, boxSize, boxSize), inventory_bg_texture,  style);

		}
	}

}
